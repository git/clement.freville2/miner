FROM python:3.12-slim

WORKDIR /app

COPY . . 
RUN pip3 install -r requirements.txt

EXPOSE 80
ENTRYPOINT ["streamlit", "run", "frontend/exploration.py", "--server.port=80", "--server.address=0.0.0.0", "--server.baseUrlPath=/containers/bastienollier-miner"]
