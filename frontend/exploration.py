import pandas as pd
import streamlit as st
import codecs

st.set_page_config(
    page_title="Project Miner",
    layout="wide"
)

st.title("Home")

### Exploration
uploaded_file = st.file_uploader("Upload your CSV file", type=["csv", "tsv"])
separator = st.selectbox("Separator", [",", ";", "\\t"])
separator = codecs.getdecoder("unicode_escape")(separator)[0]
has_header = st.checkbox("Has header", value=True)

if uploaded_file is not None:
    st.session_state.data = pd.read_csv(uploaded_file, sep=separator, header=0 if has_header else 1)
    st.session_state.original_data = st.session_state.data
    st.success("File loaded successfully!")


if "data" in st.session_state:
    data = st.session_state.data
    st.write(data.head(10))
    st.write(data.tail(10))

    st.header("Data Preview")
    
    st.subheader("First 5 Rows")
    st.write(data.head())
    
    st.subheader("Last 5 Rows")
    st.write(data.tail())
    
    st.header("Data Summary")
    
    st.subheader("Basic Information")
    col1, col2 = st.columns(2)
    col1.metric("Number of Rows", data.shape[0])
    col2.metric("Number of Columns", data.shape[1])
    
    st.write(f"Column Names: {list(data.columns)}")
    
    st.subheader("Missing Values by Column")
    missing_values = data.isnull().sum()
    st.write(missing_values)
    
    st.subheader("Statistical Summary")
    st.write(data.describe())
    
