import streamlit as st
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score,confusion_matrix
from sklearn.preprocessing import LabelEncoder
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

st.header("Prediction: Classification")

if "data" in st.session_state:
    data = st.session_state.data

    with st.form("classification_form"):
        st.subheader("Classification Parameters")
        data_name = st.multiselect("Features", data.columns)
        target_name = st.selectbox("Target", data.columns)
        test_size = st.slider("Test Size", min_value=0.1, max_value=0.5, value=0.2, step=0.1)
        st.form_submit_button('Train and Predict')

    if data_name and target_name:
        X = data[data_name]
        y = data[target_name]

        label_encoders = {}
        for column in X.select_dtypes(include=['object']).columns:
            le = LabelEncoder()
            X[column] = le.fit_transform(X[column])
            label_encoders[column] = le
        
        if y.dtype == 'object':
            le = LabelEncoder()
            y = le.fit_transform(y)
            label_encoders[target_name] = le

        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=test_size, random_state=42)

        model = LogisticRegression()
        model.fit(X_train, y_train)
        y_pred = model.predict(X_test)
        accuracy = accuracy_score(y_test, y_pred)

        st.subheader("Model Accuracy")
        st.write(f"Accuracy on test data: {accuracy:.2f}")

        st.subheader("Enter values for prediction")
        pred_values = []
        for feature in data_name:
            if feature in label_encoders:
                values = list(label_encoders[feature].classes_)
                value = st.selectbox(f"Value for {feature}", values)
                value_encoded = label_encoders[feature].transform([value])[0]
                pred_values.append(value_encoded)
            else:
                value = st.number_input(f"Value for {feature}", value=0.0)
                pred_values.append(value)
        
        prediction = model.predict(pd.DataFrame([pred_values], columns=data_name))

        if target_name in label_encoders:
            prediction = label_encoders[target_name].inverse_transform(prediction)
        
        st.write("Prediction:", prediction[0])

        if len(data_name) == 1:
            fig = plt.figure()

            y_pred = [model.predict(pd.DataFrame([pred_value[0]], columns=data_name)) for pred_value in X.values.tolist()]
            cm = confusion_matrix(y, y_pred)

            sns.heatmap(cm, annot=True, fmt="d")

            plt.xlabel('Predicted')
            plt.ylabel('True')

            st.pyplot(fig)
else:
    st.error("File not loaded")
