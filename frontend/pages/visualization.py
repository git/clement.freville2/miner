import streamlit as st
import matplotlib.pyplot as plt
import seaborn as sns

st.header("Data Visualization")


if "data" in st.session_state:
    data = st.session_state.data

    st.subheader("Histogram")
    column_to_plot = st.selectbox("Select Column for Histogram", data.columns)
    if column_to_plot:
        fig, ax = plt.subplots()
        ax.hist(data[column_to_plot].dropna(), bins=20, edgecolor='k')
        ax.set_title(f"Histogram of {column_to_plot}")
        ax.set_xlabel(column_to_plot)
        ax.set_ylabel("Frequency")
        st.pyplot(fig)
    
    st.subheader("Boxplot")
    dataNumeric = data.select_dtypes(include="number")
    column_to_plot = st.selectbox("Select Column for Boxplot", dataNumeric.columns)
    if column_to_plot:
        fig, ax = plt.subplots()
        sns.boxplot(data=data, x=column_to_plot, ax=ax)
        ax.set_title(f"Boxplot of {column_to_plot}")
        st.pyplot(fig)
else:
    st.error("file not loaded")